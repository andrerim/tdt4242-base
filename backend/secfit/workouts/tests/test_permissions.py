from django.test import RequestFactory
from django.test import TestCase

from users.models import User
from workouts.models import Workout
from workouts.permissions import IsCoachAndVisibleToCoach
from workouts.permissions import IsCoachOfWorkoutAndVisibleToCoach
from workouts.permissions import IsOwner
from workouts.permissions import IsOwnerOfWorkout
from workouts.permissions import IsPublic
from workouts.permissions import IsReadOnly
from workouts.permissions import IsWorkoutPublic


class PermissionTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        User.objects.create(password="test123", username="test@gmail.com")
        User.objects.create(password="testing1234", username="testing@gmail.com")
        user = User.objects.get(id=1)
        User.objects.create(password="coach123", username="coach@gmail.com", coach=user)
        coach = User.objects.get(id=3)
        Workout.objects.create(
            name="squat",
            date="2021-02-03T12:00:00.000000Z",
            notes="Go down to until your legs has 90 degrees.",
            owner=user,
            visibility="PU",
        )

        Workout.objects.create(
            name="squat",
            date="2021-02-03T12:00:00.000000Z",
            notes="Go down to until your legs has 90 degrees.",
            owner=coach,
            visibility="PU",
        )

        Workout.objects.create(
            name="squat",
            date="2021-02-03T12:00:00.000000Z",
            notes="Go down to until your legs has 90 degrees.",
            owner=coach,
            visibility="CO",
        )

        Workout.objects.create(
            name="squat",
            date="2021-02-03T12:00:00.000000Z",
            notes="Go down to until your legs has 90 degrees.",
            owner=coach,
            visibility="PR",
        )

    def setUp(self):
        self.user1 = User.objects.get(id=1)
        self.user2 = User.objects.get(id=2)
        self.coach = User.objects.get(id=3)
        self.workout = Workout.objects.get(id=1)
        self.workout2 = Workout.objects.get(id=2)
        self.workoutVisibilityCoach = Workout.objects.get(id=3)
        self.workoutVisibilityPrivate = Workout.objects.get(id=4)

    def test_isOwner(self):
        factory = RequestFactory()
        request = factory.get("/")
        request.user = self.user1
        workout = self.workout

        permission = IsOwner.has_object_permission(
            self, request=request, view=None, obj=workout
        )

        self.assertTrue(permission)

    def test_isNotOwner(self):
        factory = RequestFactory()
        request = factory.get("/")
        request.user = self.user2
        workout = self.workout

        permission = IsOwner.has_object_permission(
            self, request=request, view=None, obj=workout
        )
        self.assertFalse(permission)

    def test_IsOwnerOfWorkout(self):
        factory = RequestFactory()
        request = factory.post("/workout/1")
        request.user = self.user1
        request.data = {"workout": "1/"}

        permission = IsOwnerOfWorkout.has_permission(self, request=request, view=None)
        self.assertTrue(permission)

    def test_IsNotOwnerOfWorkout(self):
        factory = RequestFactory()
        request = factory.post("/workout/1")
        request.user = self.user2
        request.data = {"workout": "1/"}

        permission = IsOwnerOfWorkout.has_permission(self, request=request, view=None)
        self.assertFalse(permission)

    def test_IsOwnerOfWorkout_MissingWorkoutData(self):
        factory = RequestFactory()
        request = factory.post("/workout/1")
        request.user = self.user1
        request.data = {"work": "1/"}

        permission = IsOwnerOfWorkout.has_permission(self, request=request, view=None)
        self.assertFalse(permission)

    def test_IsOwnerOfWorkout_MissingPOSTMethod(self):
        factory = RequestFactory()
        request = factory.get("/workout/1")
        request.user = self.user2
        request.data = {"workout": "1/"}

        permission = IsOwnerOfWorkout.has_permission(self, request=request, view=None)
        self.assertTrue(permission)

    def test_IsOwnerOfWorkout_hasObjectPermission(self):
        factory = RequestFactory()
        request = factory.get("/")
        request.user = self.user1
        request.workout = self.workout

        permission = IsOwnerOfWorkout.has_object_permission(
            self, request=request, view=None, obj=request
        )

        self.assertTrue(permission)

    def test_IsOwnerOfWorkout_hasNotObjectPermission(self):
        factory = RequestFactory()
        request = factory.get("/")
        request.user = self.user2
        request.workout = self.workout

        permission = IsOwnerOfWorkout.has_object_permission(
            self, request=request, view=None, obj=request
        )

        self.assertFalse(permission)

    def test_IsCoachAndVisibleToCoach(self):
        factory = RequestFactory()
        request = factory.get("/")
        request.user = self.user1
        workout = self.workout2

        permission = IsCoachAndVisibleToCoach.has_object_permission(
            self, request=request, view=None, obj=workout
        )
        self.assertTrue(permission)

    def test_IsCoachAndVisibleToCoach_visibilityCoach(self):
        factory = RequestFactory()
        request = factory.get("/")
        request.user = self.user1
        workout = self.workoutVisibilityCoach

        permission = IsCoachAndVisibleToCoach.has_object_permission(
            self, request=request, view=None, obj=workout
        )
        self.assertTrue(permission)

    def test_IsCoachAndVisibleToCoach_visibilityPrivate(self):
        factory = RequestFactory()
        request = factory.get("/")
        request.user = self.user1
        workout = self.workoutVisibilityPrivate

        permission = IsCoachAndVisibleToCoach.has_object_permission(
            self, request=request, view=None, obj=workout
        )
        self.assertFalse(permission)

    def test_IsCoachAndVisibleToCoach_noCoachSpecified(self):
        factory = RequestFactory()
        request = factory.get("/")
        request.user = self.user2
        workout = self.workout

        permission = IsCoachAndVisibleToCoach.has_object_permission(
            self, request=request, view=None, obj=workout
        )
        self.assertFalse(permission)

    def test_IsCoachOfWorkoutAndVisibleToCoach(self):
        factory = RequestFactory()
        request = factory.get("/")
        request.workout = self.workoutVisibilityCoach
        request.user = self.user1

        permission = IsCoachOfWorkoutAndVisibleToCoach.has_object_permission(
            self, request=request, view=None, obj=request
        )
        self.assertTrue(permission)

    def test_IsCoachOfWorkoutAndVisibleToCoach_visibilityPublic(self):
        factory = RequestFactory()
        request = factory.get("/")
        request.workout = self.workout2
        request.user = self.user1

        permission = IsCoachOfWorkoutAndVisibleToCoach.has_object_permission(
            self, request=request, view=None, obj=request
        )
        self.assertTrue(permission)

    def test_IsCoachOfWorkoutAndVisibleToCoach_visibilityPrivate(self):
        factory = RequestFactory()
        request = factory.get("/")
        request.workout = self.workoutVisibilityPrivate
        request.user = self.user1

        permission = IsCoachOfWorkoutAndVisibleToCoach.has_object_permission(
            self, request=request, view=None, obj=request
        )
        self.assertFalse(permission)

    def test_IsPublic(self):
        workout = self.workout

        permission = IsPublic.has_object_permission(
            self, request=None, view=None, obj=workout
        )
        self.assertTrue(permission)

    def test_IsNotPublic(self):
        workout = self.workoutVisibilityCoach

        permission = IsPublic.has_object_permission(
            self, request=None, view=None, obj=workout
        )
        self.assertFalse(permission)

    def test_IsWorkoutPublic(self):
        workout = self.workout2
        factory = RequestFactory()
        obj = factory.get("/")
        obj.workout = workout

        permission = IsWorkoutPublic.has_object_permission(
            self, request=None, view=None, obj=obj
        )
        self.assertTrue(permission)

    def test_IsWorkoutNotPublic(self):
        workout = self.workoutVisibilityPrivate
        factory = RequestFactory()
        obj = factory.get("/")
        obj.workout = workout

        permission = IsWorkoutPublic.has_object_permission(
            self, request=None, view=None, obj=obj
        )
        self.assertFalse(permission)

    def test_IsReadOnly(self):
        factory = RequestFactory()
        request = factory.get("/")

        permission = IsReadOnly.has_object_permission(
            self, request=request, view=None, obj=None
        )
        self.assertTrue(permission)

    def test_IsNotReadOnly(self):
        factory = RequestFactory()
        request = factory.post("/")

        permission = IsReadOnly.has_object_permission(
            self, request=request, view=None, obj=None
        )
        self.assertFalse(permission)
