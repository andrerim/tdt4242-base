from rest_framework import serializers
from rest_framework.serializers import HyperlinkedRelatedField

from workouts.models import Exercise
from workouts.models import ExerciseInstance
from workouts.models import Workout
from workouts.models import WorkoutFile


class ExerciseInstanceSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an ExerciseInstance. Hyperlinks are used for
    relationships by default.

    Serialized fields: url, id, exercise, sets, number, workout

    Attributes:
        workout:    The associated workout for this instance, represented by a hyperlink
    """

    workout = HyperlinkedRelatedField(
        queryset=Workout.objects.all(), view_name="workout-detail", required=False
    )

    class Meta:
        model = ExerciseInstance
        fields = ["url", "id", "exercise", "sets", "number", "workout"]


class WorkoutFileSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a WorkoutFile. Hyperlinks are used for relationships by
    default.

    Serialized fields: url, id, owner, file, workout

    Attributes:
        owner:      The owner (User) of the WorkoutFile, represented by a username. ReadOnly
        workout:    The associate workout for this WorkoutFile, represented by a hyperlink
    """

    owner = serializers.ReadOnlyField(source="owner.username")
    workout = HyperlinkedRelatedField(
        queryset=Workout.objects.all(), view_name="workout-detail", required=False
    )

    class Meta:
        model = WorkoutFile
        fields = ["url", "id", "owner", "file", "workout"]

    def create(self, validated_data):
        return WorkoutFile.objects.create(**validated_data)


class WorkoutSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a Workout. Hyperlinks are used for relationships by
    default.

    This serializer specifies nested serialization since a workout consists of WorkoutFiles
    and ExerciseInstances.

    Serialized fields: url, id, name, date, notes, owner, owner_username, visiblity,
                       exercise_instances, files

    Attributes:
        owner_username:     Username of the owning User
        exercise_instance:  Serializer for ExericseInstances
        files:              Serializer for WorkoutFiles
    """

    owner_username = serializers.SerializerMethodField()
    exercise_instances = ExerciseInstanceSerializer(many=True, required=True)
    files = WorkoutFileSerializer(many=True, required=False)

    class Meta:
        model = Workout
        fields = [
            "url",
            "id",
            "name",
            "date",
            "notes",
            "owner",
            "owner_username",
            "visibility",
            "exercise_instances",
            "files",
        ]
        extra_kwargs = {"owner": {"read_only": True}}

    def create(self, validated_data):
        """Custom logic for creating ExerciseInstances, WorkoutFiles, and a
        Workout.

        This is needed to iterate over the files and exercise instances, since this serializer is
        nested.

        Args:
            validated_data: Validated files and exercise_instances

        Returns:
            Workout: A newly created Workout
        """
        exercise_instances_data = validated_data.pop("exercise_instances")
        files_data = []
        if "files" in validated_data:
            files_data = validated_data.pop("files")

        workout = Workout.objects.create(**validated_data)

        for exercise_instance_data in exercise_instances_data:
            ExerciseInstance.objects.create(workout=workout, **exercise_instance_data)
        for file_data in files_data:
            WorkoutFile.objects.create(
                workout=workout, owner=workout.owner, file=file_data.get("file")
            )

        return workout

    def update(self, instance, validated_data):
        """Custom logic for updating a Workout with its ExerciseInstances and
        Workouts.

        This is needed because each object in both exercise_instances and files must be iterated
        over and handled individually.

        Args:
            instance (Workout): Current Workout object
            validated_data: Contains data for validated fields

        Returns:
            Workout: Updated Workout instance
        """
        if data := validated_data.pop("exercise_instances"):
            self.update_workout_exercises(instance, data)

        for attr in ["name", "notes", "visibility", "date"]:
            if attr in validated_data:
                setattr(instance, attr, validated_data[attr])

        if data := validated_data.pop("files"):
            self.update_workout_files(instance, data)

        instance.save()
        return instance

    def update_workout_exercises(self, workout, data):
        """Updates the exercise instances associated with a workout.

        Args:
            workout (Workout): Current workout object.
            data (list): A list of validated exercise instance data.
        """
        exercises = workout.exercise_instances.all()

        for exercise, exercise_data in zip(exercises, data):
            for attr in ["exercise", "number", "sets"]:
                if attr in exercise_data:
                    setattr(exercise, attr, exercise_data[attr])
            exercise.save()

        num_data = len(data)
        num_exercises = len(exercises)

        if num_data > num_exercises:
            for i in range(num_exercises, num_data):
                ExerciseInstance.objects.create(
                    workout=workout,
                    **data[i],
                )
        elif num_data < num_exercises:
            for i in range(num_data, num_exercises):
                exercises[i].delete()

    def update_workout_files(self, workout, data):
        """Updates the files associated with a workout.

        Args:
            workout (Workout): Current workout object.
            data (list): A list of validated file data.
        """
        files = workout.files.all()

        for file_obj, file_data in zip(files, data):
            if file := file_data.get("file"):
                file_obj.file = file
            file_obj.save()

        num_data = len(data)
        num_files = len(files)

        if num_data > num_files:
            for i in range(num_files, num_data):
                WorkoutFile.objects.create(
                    workout=workout,
                    owner=workout.owner,
                    **data[i],
                )
        elif num_data < num_files:
            for i in range(num_data, num_files):
                files[i].delete()

    def get_owner_username(self, obj):
        """Returns the owning user's username.

        Args:
            obj (Workout): Current Workout

        Returns:
            str: Username of owner
        """
        return obj.owner.username


class ExerciseSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an Exercise. Hyperlinks are used for relationships by
    default.

    Serialized fields: url, id, name, description, unit, instances

    Attributes:
        instances:  Associated exercise instances with this Exercise type. Hyperlinks.
    """

    instances = serializers.HyperlinkedRelatedField(
        many=True, view_name="exerciseinstance-detail", read_only=True
    )

    class Meta:
        model = Exercise
        fields = ["url", "id", "name", "description", "unit", "instances"]


class HighScoreSerializer(serializers.Serializer):
    """Serializer for a HighScore.

    Attributes:
        score: The number of workouts completed.
        units: The number of units performed for an exercise.
        username: The username of the athlete holding this high score.
        workouts: The list of workouts related to this high score.
    """

    score = serializers.IntegerField()
    units = serializers.IntegerField()
    username = serializers.SerializerMethodField()
    workouts = WorkoutSerializer(many=True)

    def get_username(self, highscore):
        """Returns the athlete's username."""
        return highscore.athlete.username
