from django.contrib import admin

from .models import Exercise
from .models import ExerciseInstance
from .models import Workout
from .models import WorkoutFile

admin.site.register(Exercise)
admin.site.register(ExerciseInstance)
admin.site.register(Workout)
admin.site.register(WorkoutFile)
