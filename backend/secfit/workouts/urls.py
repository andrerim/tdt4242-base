from django.urls import path

from workouts import views

urlpatterns = [
    path("workouts/", views.WorkoutList.as_view(), name="workout-list"),
    path(
        "workouts/<int:pk>/",
        views.WorkoutDetail.as_view(),
        name="workout-detail",
    ),
    path("highscores/", views.HighScoreList.as_view(), name="highscore-list"),
    path("exercises/", views.ExerciseList.as_view(), name="exercise-list"),
    path(
        "exercises/<int:pk>/",
        views.ExerciseDetail.as_view(),
        name="exercise-detail",
    ),
    path(
        "exercise-instances/",
        views.ExerciseInstanceList.as_view(),
        name="exercise-instance-list",
    ),
    path(
        "exercise-instances/<int:pk>/",
        views.ExerciseInstanceDetail.as_view(),
        name="exerciseinstance-detail",
    ),
    path(
        "workout-files/",
        views.WorkoutFileList.as_view(),
        name="workout-file-list",
    ),
    path(
        "workout-files/<int:pk>/",
        views.WorkoutFileDetail.as_view(),
        name="workoutfile-detail",
    ),
]
