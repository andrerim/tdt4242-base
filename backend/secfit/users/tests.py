from django.test import TestCase
from rest_framework.serializers import ValidationError

from users.serializers import UserSerializer


def create_user_data(**kwargs):
    data = {
        "username": "",
        "email": "",
        "password": "",
        "password1": "",
        "phone_number": "",
        "country": "",
        "city": "",
        "street_address": "",
    }
    for key, val in kwargs.items():
        if key in data:
            data[key] = val
    return data


class UserSerializerTest(TestCase):
    def setUp(self):
        self.empty_data = create_user_data()
        self.valid_data = create_user_data(
            username="tyrion",
            email="tyrion@lannister.com",
            password="iDrInKaNdIkNoWtHiNgS",
            password1="iDrInKaNdIkNoWtHiNgS",
            phone_number="+4791827364",
            country="Crownlands",
            city="King's Landing",
            street_address="The Red Keep 404",
        )
        self.min_valid_data = create_user_data(
            username="johndoe",
            password="sUpErSeCrEt",
            password1="sUpErSeCrEt",
        )

    def test_valid_data(self):
        for data in [self.valid_data, self.min_valid_data]:
            serializer = UserSerializer(data=data)
            self.assertTrue(serializer.is_valid())

    def test_empty_data(self):
        serializer = UserSerializer(data=self.empty_data)
        self.assertFalse(serializer.is_valid())

    def test_empty_username(self):
        data = create_user_data(
            username="", password="sUpErSeCrEt", password1="sUpErSeCrEt"
        )
        serializer = UserSerializer(data=data)
        self.assertFalse(serializer.is_valid())

    def test_empty_passwords(self):
        data = create_user_data(username="johndoe")
        serializer = UserSerializer(data=data)
        self.assertFalse(serializer.is_valid())

    def test_empty_password1(self):
        data = create_user_data(
            username="johndoe",
            password="sUpErSeCrEt",
        )
        serializer = UserSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        with self.assertRaises(ValidationError):
            serializer.validate_password(data["password"])

    def test_empty_password(self):
        data = create_user_data(
            username="johndoe",
            password1="sUpErSeCrEt",
        )
        serializer = UserSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        with self.assertRaises(ValidationError):
            serializer.validate_password(data["password"])

    def test_passwords_do_not_match(self):
        data = create_user_data(
            username="johndoe", password="sUpErSeCrEt", password1="dOeSnOtMaTcH"
        )
        serializer = UserSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        with self.assertRaises(ValidationError):
            serializer.validate_password(data["password"])

    def test_too_short_password(self):
        data = create_user_data(
            username="johndoe",
            password="tOoShOrT",
            password1="tOoShOrT",
        )
        serializer = UserSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        with self.assertRaises(ValidationError):
            serializer.validate_password(data["password"])

    def test_create_user(self):
        data = self.valid_data
        serializer = UserSerializer(data=self.valid_data)
        self.assertTrue(serializer.is_valid())
        validated_data = serializer.validated_data
        user = serializer.create(validated_data)
        self.assertIsNotNone(user.password)
        self.assertFalse(hasattr(user, "password1"))
        for attr, value in data.items():
            if attr not in ["password", "password1"]:
                self.assertEqual(getattr(user, attr), value)
