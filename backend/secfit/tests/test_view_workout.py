import os
import pathlib

from selenium.webdriver.common.by import By

from tests.common import BlackBoxTestServer
from tests.common import login_data
from tests.common import Visibility

here = pathlib.Path(__file__).parent.absolute()
data_dir = here / "data"
backend_port = os.environ.get("BACKEND_PORT", 8000)
frontend_port = os.environ.get("FRONTEND_PORT", 8001)
frontend_address = f"http://localhost:{frontend_port}"


workout_input_ids = (
    "inputName",
    "inputDateTime",
    "inputOwner",
    "inputVisibility",
    "inputNotes",
)


class ViewWorkoutTest(BlackBoxTestServer):
    """Test case for FR5 View Workout.

    Requirement description:

    The user should be able to view all of the details, files, and comments
    on workouts of sufficient visibility. For athletes, this means that the
    workout needs to be either their own or public. For coaches, this means
    that the workout is at least one of their athletes non-private workouts
    OR the workout is public. For visitors, this means that the workout
    needs to be public.
    """

    headless = True
    fixtures = [
        data_dir / "users.json",
        data_dir / "exercises.json",
        data_dir / "workouts.json",
    ]

    def wait_until_value_not_empty(self, by: By, locator: str):
        self.wait.until(lambda _: self.get_input_value(by, locator) != "")

    def get_input_value(self, by: By, locator: str) -> str:
        element = self.driver.find_element(by, locator)
        self.assertIsNotNone(element)
        return element.get_attribute("value")

    def test_user_can_view_own_public_workout(self):
        """Test if a user is able to view their own public workout."""
        user = login_data["athlete"]
        self.login(user)
        self.get("workout.html", id=1)
        self.assertEqual("Workout", self.driver.title)
        self.wait_until_value_not_empty(By.ID, "inputOwner")
        for input_id in workout_input_ids:
            element = self.driver.find_element_by_id(input_id)
            value = element.get_attribute("value")
            self.assertIsNotNone(value)
            if input_id == "inputOwner":
                self.assertEqual(value, user.username)
            if input_id == "inputVisibility":
                self.assertEqual(value, Visibility.PUBLIC)

    def test_coach_can_view_athletes_workout(self):
        """Test if a coach is able to view their athlete's workout."""
        user = login_data["coach"]
        self.login(user)
        self.get("workout.html", id=2)
        self.assertEqual("Workout", self.driver.title)
        self.wait_until_value_not_empty(By.ID, "inputOwner")
        for input_id in workout_input_ids:
            value = self.get_input_value(By.ID, input_id)
            self.assertIsNotNone(value)
            if input_id == "inputOwner":
                self.assertNotEqual(value, user.username)
            if input_id == "inputVisibility":
                self.assertEqual(value, Visibility.COACH)

    def test_user_can_view_others_public_workout(self):
        """Test if a user is able to view another users' public workout."""
        user = login_data["athlete"]
        self.login(user)
        self.get("workout.html", id=3)
        self.assertEqual("Workout", self.driver.title)
        self.wait_until_value_not_empty(By.ID, "inputOwner")
        for input_id in workout_input_ids:
            value = self.get_input_value(By.ID, input_id)
            self.assertIsNotNone(value)
            if input_id == "inputOwner":
                self.assertNotEqual(value, user.username)
            if input_id == "inputVisibility":
                self.assertEqual(value, Visibility.PUBLIC)

    def test_user_can_view_own_private_workout(self):
        """Test if a user is able to view their own private workout."""
        user = login_data["athlete"]
        self.login(user)
        self.get("workout.html", id=4)
        self.assertEqual("Workout", self.driver.title)
        self.wait_until_value_not_empty(By.ID, "inputOwner")
        for input_id in workout_input_ids:
            value = self.get_input_value(By.ID, input_id)
            self.assertIsNotNone(value)
            if input_id == "inputOwner":
                self.assertEqual(value, user.username)
            if input_id == "inputVisibility":
                self.assertEqual(value, Visibility.PRIVATE)

    def test_user_cannot_view_other_users_private_workout(self):
        """Test if a user is unable to view another users' private workout."""
        user = login_data["coach"]
        self.login(user)
        self.get("workout.html", id=4)
        self.assertEqual("Workout", self.driver.title)
        alert = self.driver.find_elements_by_xpath("//div[@role='alert']")
        self.assertIsNotNone(alert)
        for input_id in workout_input_ids:
            # For workouts that don't exist or have insufficient visibility,
            # the form input elements will be populated with empty values.
            # Except for the visibility field, which will have "PU" as default
            # value regardess. It therefor makes no sense to check its value.
            if input_id != "inputVisibility":
                value = self.get_input_value(By.ID, input_id)
                self.assertEqual(value, "")
