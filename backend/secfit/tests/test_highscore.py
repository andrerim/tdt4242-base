import os
import pathlib
import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

from tests.common import BlackBoxTestServer
from tests.common import login_data

here = pathlib.Path(__file__).parent.absolute()
data_dir = here / "data"
backend_port = os.environ.get("BACKEND_PORT", 8000)
frontend_port = os.environ.get("FRONTEND_PORT", 8001)
frontend_address = f"http://localhost:{frontend_port}"


class FilterWorkoutsTest(BlackBoxTestServer):

    headless = True
    fixtures = [
        data_dir / "users.json",
        data_dir / "exercises.json",
        data_dir / "workouts.json",
    ]

    def wait_until_value_not_empty(self, by: By, locator: str):
        self.wait.until(lambda _: self.get_input_value(by, locator) != "")

    def get_input_value(self, by: By, locator: str) -> str:
        element = self.driver.find_element(by, locator)
        self.assertIsNotNone(element)
        return element.get_attribute("value")

    def test_user_can_view_highscores(self):
        user = login_data["athlete"]
        self.login(user)
        self.get("highscores.html")
        self.assertEqual("Highscores", self.driver.title)
        time.sleep(2)
        highscores = self.driver.find_elements_by_css_selector("#highscore-content>tr")
        self.assertEqual(len(highscores), 2)

    def test_highscore_ranking(self):
        user = login_data["athlete"]
        self.login(user)
        self.get("highscores.html")
        self.assertEqual("Highscores", self.driver.title)
        time.sleep(2)
        highscores = self.driver.find_elements_by_css_selector("#highscore-content>tr")
        self.assertEqual(highscores[0].text, "1 arya 2")
        self.assertEqual(highscores[1].text, "2 syrio 1")

    def test_filter_highscore_on_exercise(self):
        user = login_data["athlete"]
        self.login(user)
        self.get("highscores.html")
        self.assertEqual("Highscores", self.driver.title)
        time.sleep(2)
        select = Select((self.driver.find_element_by_id("filter-exercise")))
        select.select_by_visible_text("Fencing")
        time.sleep(2)
        highscores = self.driver.find_elements_by_css_selector("#highscore-content>tr")
        self.assertEqual(len(highscores), 1)
        self.assertEqual(highscores[0].text, "1 arya 1 24")
