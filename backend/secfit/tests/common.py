import enum
import os
from dataclasses import dataclass
from urllib.parse import urljoin

from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

backend_port = os.environ.get("BACKEND_PORT", 8000)
frontend_port = os.environ.get("FRONTEND_PORT", 8001)
frontend_address = f"http://localhost:{frontend_port}"
webdriver_path = os.environ.get("WEBDRIVER", "chromedriver")


class Visibility(str, enum.Enum):
    """Enum representing workout visibility."""

    PUBLIC = "PU"
    COACH = "CO"
    PRIVATE = "PR"


@dataclass
class LoginData(dict):
    username: str
    password: str


login_data = {
    "athlete": LoginData("arya", "dOnOtCaLlMeMiLaDy"),
    "coach": LoginData("syrio", "wHaTdOwEsAyToThEgOdOfDeAtH"),
}


class BlackBoxTestServer(LiveServerTestCase):
    """Base class for running black box tests with Selenium.

    The frontend needs be started before running the tests. The frontend port
    can be specified using the environment variable FRONTEND_PORT. If this
    environment variable is not present, the test case expect the frontend to
    be running on port 8001.

    Likewise, the port for the backend test server can be specified using the
    BACKEND_PORT environment variable, which defaults to 8000.

    Examples:

    The following examples assumes you stand in the root folder of repo.

    Run tests with default values:

        (cd frontend && cordova run browser --port=8001)
        (cd backend/secfit && python manage.py test tests)

    Run tests with backend listening on port 12345:

        (
            cd frontend &&
            BACKEND_HOST=http://localhost:12345 \
                cordova run browser --port=8001
        )
        (
            cd backend/secfit &&
            BACKEND_PORT=12345 python manage.py test tests
        )
    """

    port = backend_port
    timeout = 5
    headless = False

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        options = webdriver.ChromeOptions()
        options.headless = cls.headless
        cls.driver = webdriver.Chrome(webdriver_path, options=options)
        cls.wait = WebDriverWait(cls.driver, cls.timeout)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()

    @property
    def address(self):
        return frontend_address

    def get(self, page: str, **params):
        """Navigate to a given web page.

        Args:
            page (str): The web page to navigate to.
            **params: Arbitrary keyword arguments used to create and attach a
                query string to the web page URL.
        """
        url = urljoin(self.address, page)
        if params:
            query_str = "&".join([f"{k}={v}" for k, v in params.items()])
            url = f"{url}?{query_str}"
        self.driver.get(url)

    def login(self, user: LoginData):
        self.get("login.html")
        self.assertEqual("Login", self.driver.title)
        username_input = self.driver.find_element_by_name("username")
        username_input.send_keys(user.username)
        password_input = self.driver.find_element_by_name("password")
        password_input.send_keys(user.password)
        self.driver.find_element_by_id("btn-login").click()
        self.wait.until(lambda driver: driver.title == "Workouts")

    def logout(self):
        self.get("logout.html")
        self.assertEqual("Logout", self.driver.title)
        self.wait.until(lambda driver: driver.title == "Home")
