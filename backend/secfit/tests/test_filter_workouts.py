import os
import pathlib
import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

from tests.common import BlackBoxTestServer
from tests.common import login_data

here = pathlib.Path(__file__).parent.absolute()
data_dir = here / "data"
backend_port = os.environ.get("BACKEND_PORT", 8000)
frontend_port = os.environ.get("FRONTEND_PORT", 8001)
frontend_address = f"http://localhost:{frontend_port}"


class FilterWorkoutsTest(BlackBoxTestServer):

    headless = True
    fixtures = [
        data_dir / "users.json",
        data_dir / "exercises.json",
        data_dir / "workouts.json",
    ]

    def wait_until_value_not_empty(self, by: By, locator: str):
        self.wait.until(lambda _: self.get_input_value(by, locator) != "")

    def get_input_value(self, by: By, locator: str) -> str:
        element = self.driver.find_element(by, locator)
        self.assertIsNotNone(element)
        return element.get_attribute("value")

    def test_exercise_filter_is_populated(self):
        user = login_data["athlete"]
        self.login(user)
        self.get("workouts.html")
        self.assertEqual("Workouts", self.driver.title)
        time.sleep(2)
        self.get_input_value(By.ID, "filter-exercise")
        options = self.driver.find_elements_by_tag_name("option")
        self.assertEqual(len(options), 3)
        for option in options:
            self.assertIn(option.text, ["", "Fencing", "Stick fighting"])

    def test_user_can_filter_workouts(self):
        user = login_data["athlete"]
        self.login(user)
        self.get("workouts.html")
        self.assertEqual("Workouts", self.driver.title)
        time.sleep(2)
        options = self.driver.find_elements_by_class_name("workout")
        self.assertEqual(len(options), 5)
        select = Select((self.driver.find_element_by_id("filter-exercise")))
        select.select_by_visible_text("Fencing")
        time.sleep(2)
        filtered_workouts = self.driver.find_elements_by_css_selector(
            ".workout:not(.hide)"
        )
        self.assertEqual(len(filtered_workouts), 2)
