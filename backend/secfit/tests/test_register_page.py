import os
from pathlib import Path

from django.contrib.auth import get_user_model
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait


def write_results_from_tests(filePath, content):
    resultDir = filePath.parent
    if not resultDir.exists():
        os.makedirs(resultDir)
    file = open(filePath, "a")
    file.write(content)
    file.close()


def format_result_string(id, name, boolean, data):
    return (
        "Test ID: "
        + id
        + "\nTest name: "
        + name
        + "\nTest result: "
        + boolean
        + "\nFailed dataset:"
        + "\n username: "
        + data["username"]
        + "\n email: "
        + data["email"]
        + "\n password:"
        + data["password"]
        + "\n password1:"
        + data["password1"]
        + "\n phone number:"
        + data["phone_number"]
        + "\n country:"
        + data["country"]
        + "\n city:"
        + data["city"]
        + "\n street_address:"
        + data["street_address"]
        + "\n\n"
    )


def create_test_set(**kwargs):
    data = {
        "username": "",
        "email": "",
        "password": "",
        "password1": "",
        "phone_number": "",
        "country": "",
        "city": "",
        "street_address": "",
    }

    for key, val in kwargs.items():
        if key in data:
            data[key] = val
    return data


class TwoWayDomainTest(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        cls.port = 8000
        super().setUpClass()
        cls.WAIT_TIMEOUT = 2
        cls.driver = webdriver.Chrome()
        cls.wait = WebDriverWait(cls.driver, cls.WAIT_TIMEOUT)

        cls.data_folder = Path(os.getcwd() + "/tests/results/")
        cls.filePath = cls.data_folder / "two_way_domain_failed_results.txt"
        cls.filePath1 = cls.data_folder / "two_way_domain_results.txt"
        cls.url = "http://localhost:8001/register.html"

        cls.a1 = "a2."
        cls.a2 = "a" * 7 + "2."
        cls.b1 = "a2!@post.no"
        cls.c1 = "a" * 7 + "2!"
        cls.c2 = "a" * 30
        cls.d1 = "a" * 7 + "2!"
        cls.d2 = "a" * 30
        cls.e1 = "12345678"
        cls.e2 = "123456789" + "0" * 10
        cls.f1 = "a" * 5
        cls.g1 = "a" * 4
        cls.h1 = "a" * 3

    def setUp(self):
        self.delete_user()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()

    def get_input_field_and_populate_data(self, **kwargs):
        for key, val in kwargs.items():
            input_field = self.driver.find_element_by_name(key)
            input_field.send_keys(val)

    def delete_user(self):
        for username in [self.a1, self.a2]:
            user = get_user_model().objects.filter(username=username).first()
            if user:
                user.delete()

    def navigate_to_register_page_and_validate_navigation(self):
        self.driver.get(self.url)
        pageTitle = self.driver.title
        self.assertEqual(
            pageTitle,
            "Register",
            "This test should pass when title of page is Register.",
        )

    def click_submit_button(self):
        self.driver.find_element_by_id("btn-create-account").click()

    def test_navigated_to_register_page(self):
        self.navigate_to_register_page_and_validate_navigation()

    def test_all_string_values(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a1,
            email=self.b1,
            password=self.c1,
            password1=self.d1,
            phone_number=self.e1,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string("1", "test_all_string_values", "True", data)
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string("1", "test_all_string_values", "False", data)
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_wrong_phone_number_length(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a1,
            email=self.b1,
            password=self.c1,
            password1=self.d1,
            phone_number=self.e2,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "2",
                "test_all_string_values_with_wrong_phone_number_length",
                "True",
                data,
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "2",
                "test_all_string_values_with_wrong_phone_number_length",
                "False",
                data,
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_password1_length(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a1,
            email=self.b1,
            password=self.c1,
            password1=self.d2,
            phone_number=self.e1,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "3", "test_all_string_values_with_password1_length", "True", data
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "3", "test_all_string_values_with_password1_length", "False", data
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_password_length(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a1,
            email=self.b1,
            password=self.c2,
            password1=self.d1,
            phone_number=self.e1,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "4", "test_all_string_values_with_password_length", "True", data
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "4", "test_all_string_values_with_password_length", "False", data
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_username_length(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a2,
            email=self.b1,
            password=self.c1,
            password1=self.d1,
            phone_number=self.e1,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "5", "test_all_string_values_with_username_length", "True", data
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "5", "test_all_string_values_with_username_length", "False", data
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_combining_wrong_phone_number_length_and_password1_length(
        self,
    ):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a1,
            email=self.b1,
            password=self.c1,
            password1=self.d2,
            phone_number=self.e2,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "6",
                "test_all_string_values_with_combining_wrong_phone_number_length_and_password1_length",
                "True",
                data,
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "6",
                "test_all_string_values_with_combining_wrong_phone_number_length_and_password1_length",
                "False",
                data,
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_combining_wrong_phone_number_length_and_password_length(
        self,
    ):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a1,
            email=self.b1,
            password=self.c2,
            password1=self.d1,
            phone_number=self.e2,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "7",
                "test_all_string_values_with_combining_wrong_phone_number_length_and_password_length",
                "True",
                data,
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "7",
                "test_all_string_values_with_combining_wrong_phone_number_length_and_password_length",
                "False",
                data,
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_combining_wrong_phone_number_length_and_username_length(
        self,
    ):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a2,
            email=self.b1,
            password=self.c1,
            password1=self.d1,
            phone_number=self.e2,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "8",
                "test_all_string_values_with_combining_wrong_phone_number_length_and_username_length",
                "True",
                data,
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "8",
                "test_all_string_values_with_combining_wrong_phone_number_length_and_username_length",
                "False",
                data,
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_combining_password1_length_and_password_length(
        self,
    ):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a1,
            email=self.b1,
            password=self.c2,
            password1=self.d2,
            phone_number=self.e1,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "9",
                "test_all_string_values_with_combining_password1_length_and_password_length",
                "True",
                data,
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "9",
                "test_all_string_values_with_combining_password1_length_and_password_length",
                "False",
                data,
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_combining_password1_length_and_username_length(
        self,
    ):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a2,
            email=self.b1,
            password=self.c1,
            password1=self.d2,
            phone_number=self.e1,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "10",
                "test_all_string_values_with_combining_password1_length_and_username_length",
                "True",
                data,
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "10",
                "test_all_string_values_with_combining_password1_length_and_username_length",
                "False",
                data,
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_combining_password_length_and_username_length(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a2,
            email=self.b1,
            password=self.c2,
            password1=self.d1,
            phone_number=self.e1,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "11",
                "test_all_string_values_with_combining_password_length_and_username_length",
                "True",
                data,
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "11",
                "test_all_string_values_with_combining_password_length_and_username_length",
                "False",
                data,
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_combining_wrong_phone_number_length_password1_length_and_username_length(
        self,
    ):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a2,
            email=self.b1,
            password=self.c1,
            password1=self.d2,
            phone_number=self.e2,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "12",
                "test_all_string_values_with_combining_wrong_phone_number_length_password1_length_and_username_length",
                "True",
                data,
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "12",
                "test_all_string_values_with_combining_wrong_phone_number_length_password1_length_and_username_length",
                "False",
                data,
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_combining_wrong_phone_number_length_password_length_and_password1_length(
        self,
    ):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a1,
            email=self.b1,
            password=self.c2,
            password1=self.d2,
            phone_number=self.e2,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "13",
                "test_all_string_values_with_combining_wrong_phone_number_length_password_length_and_password1_length",
                "True",
                data,
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "13",
                "test_all_string_values_with_combining_wrong_phone_number_length_password_length_and_password1_length",
                "False",
                data,
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_with_combining_username_length_password_length_and_password1_length(
        self,
    ):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a2,
            email=self.b1,
            password=self.c2,
            password1=self.d2,
            phone_number=self.e1,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "14",
                "test_all_string_values_with_combining_username_length_password_length_and_password1_length",
                "True",
                data,
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "14",
                "test_all_string_values_with_combining_username_length_password_length_and_password1_length",
                "False",
                data,
            )
            write_results_from_tests(self.filePath, content)

    def test_all_string_values_second_value(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.a2,
            email=self.b1,
            password=self.c2,
            password1=self.d2,
            phone_number=self.e2,
            country=self.f1,
            city=self.g1,
            street_address=self.h1,
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string(
                "15", "test_all_string_values_second_value", "True", data
            )
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "15", "test_all_string_values_second_value", "False", data
            )
            write_results_from_tests(self.filePath, content)


class BoundryValueRegisterTest(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        cls.port = 8000
        super().setUpClass()
        cls.WAIT_TIMEOUT = 2
        cls.driver = webdriver.Chrome()
        cls.wait = WebDriverWait(cls.driver, cls.WAIT_TIMEOUT)

        cls.data_folder = Path(os.getcwd() + "/tests/results/")
        cls.filePath1 = cls.data_folder / "boundry_value_results.txt"
        cls.filePath = cls.data_folder / "boundry_value_failed_results.txt"
        cls.url = "http://localhost:8001/register.html"

        cls.dataSet = {
            "empty": {
                "username": "",
                "password": "",
                "password1": "",
                "email": "",
                "phone_number": "",
                "country": "",
                "city": "",
                "street_address": "",
            },
            "valid_range_1": {
                "username": "a",
                "password": "a" * 9,
                "password1": "a" * 9,
                "email": "post@secfit.no",
                "phone_number": "+4712345678",
                "country": "Norge",
                "city": "Trondheim",
                "street_address": "Høyskoleringen 3",
            },
            "valid_range_2": {
                "username": "a" * 30,
                "password": "a" * 100,
                "password1": "a" * 100,
                "email": "p" + ("o" * 100) + "st@secfit.no",
                "phone_number": "+471234567800",
                "country": "Norge",
                "city": "Trondheim",
                "street_address": "Høyskoleringen 3",
            },
            "invalid_range": {
                "username": "a" * 80,
                "password": "a" * 7,
                "password1": "a" * 7,
                "email": "post@secfit.n" + "o" * 200,
                "phone_number": ".471234567800000000000000",
                "country": "Norge" * 200,
                "city": "Trondheim" * 200,
                "street_address": "Høyskoleringen 3" * 130,
            },
            "only_numbers": {
                "username": "0" * 20,
                "password": "0" * 40,
                "password1": "0" * 40,
                "email": "1" * 40,
                "phone_number": "4712345678" + "1" * 20,
                "country": "3456678",
                "city": "3090",
                "street_address": "200000000100",
            },
            "only_special_chars": {
                "username": "! ::_-@",
                "password": " ?" * 20,
                "password1": " ?" * 20,
                "email": "@" * 20,
                "phone_number": "+&" * 20,
                "country": "!" * 20,
                "city": "/" * 20,
                "street_address": ":" * 20,
            },
            "extremely_long_input": {
                "username": "a2!" * 500,
                "password": "a2!" * 500,
                "password1": "a2!" * 500,
                "email": "p" * 500 + "ost@secfit.no",
                "phone_number": "+471234567" + "8" * 500,
                "country": "Norg" + "e" * 500,
                "city": "Trondhei" + "m" * 500,
                "street_address": "Høyskoleringen" + "3" * 500,
            },
        }

    def setUp(self):
        pass

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()

    def get_input_field_and_populate_data(self, **kwargs):
        for key, val in kwargs.items():
            input_field = self.driver.find_element_by_name(key)
            input_field.send_keys(val)

    def click_submit_button(self):
        self.driver.find_element_by_id("btn-create-account").click()

    def navigate_to_register_page_and_validate_navigation(self):
        self.driver.get(self.url)
        pageTitle = self.driver.title
        self.assertEqual(
            pageTitle,
            "Register",
            "This test should pass when title of page is Register.",
        )

    def test_navigated_to_register_page(self):
        self.navigate_to_register_page_and_validate_navigation()

    def test_empty_input(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.dataSet["empty"]["username"],
            password=self.dataSet["empty"]["password"],
            password1=self.dataSet["empty"]["password1"],
            email=self.dataSet["empty"]["email"],
            phone_number=self.dataSet["empty"]["phone_number"],
            country=self.dataSet["empty"]["country"],
            city=self.dataSet["empty"]["city"],
            street_address=self.dataSet["empty"]["street_address"],
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string("1", "test_empty_input", "True", data)
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string("1", "test_empty_input", "False", data)
            write_results_from_tests(self.filePath, content)

    def test_valid_range_1(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.dataSet["valid_range_1"]["username"],
            password=self.dataSet["valid_range_1"]["password"],
            password1=self.dataSet["valid_range_1"]["password1"],
            email=self.dataSet["valid_range_1"]["email"],
            phone_number=self.dataSet["valid_range_1"]["phone_number"],
            country=self.dataSet["valid_range_1"]["country"],
            city=self.dataSet["valid_range_1"]["city"],
            street_address=self.dataSet["valid_range_1"]["street_address"],
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string("2", "test_valid_range_1", "True", data)
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string("2", "test_valid_range_1", "False", data)
            write_results_from_tests(self.filePath, content)

    def test_valid_range_2(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.dataSet["valid_range_2"]["username"],
            password=self.dataSet["valid_range_2"]["password"],
            password1=self.dataSet["valid_range_2"]["password1"],
            email=self.dataSet["valid_range_2"]["email"],
            phone_number=self.dataSet["valid_range_2"]["phone_number"],
            country=self.dataSet["valid_range_2"]["country"],
            city=self.dataSet["valid_range_2"]["city"],
            street_address=self.dataSet["valid_range_2"]["street_address"],
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string("3", "test_valid_range_2", "True", data)
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string("3", "test_valid_range_2", "False", data)
            write_results_from_tests(self.filePath, content)

    def test_invalid_range(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.dataSet["invalid_range"]["username"],
            password=self.dataSet["invalid_range"]["password"],
            password1=self.dataSet["invalid_range"]["password1"],
            email=self.dataSet["invalid_range"]["email"],
            phone_number=self.dataSet["invalid_range"]["phone_number"],
            country=self.dataSet["invalid_range"]["country"],
            city=self.dataSet["invalid_range"]["city"],
            street_address=self.dataSet["invalid_range"]["street_address"],
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string("4", "test_invalid_range", "True", data)
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string("4", "test_invalid_range", "False", data)
            write_results_from_tests(self.filePath, content)

    def test_only_numbers(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.dataSet["only_numbers"]["username"],
            password=self.dataSet["only_numbers"]["password"],
            password1=self.dataSet["only_numbers"]["password1"],
            email=self.dataSet["only_numbers"]["email"],
            phone_number=self.dataSet["only_numbers"]["phone_number"],
            country=self.dataSet["only_numbers"]["country"],
            city=self.dataSet["only_numbers"]["city"],
            street_address=self.dataSet["only_numbers"]["street_address"],
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string("5", "test_only_numbers", "True", data)
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string("5", "test_only_numbers", "False", data)
            write_results_from_tests(self.filePath, content)

    def test_only_special_chars(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.dataSet["only_special_chars"]["username"],
            password=self.dataSet["only_special_chars"]["password"],
            password1=self.dataSet["only_special_chars"]["password1"],
            email=self.dataSet["only_special_chars"]["email"],
            phone_number=self.dataSet["only_special_chars"]["phone_number"],
            country=self.dataSet["only_special_chars"]["country"],
            city=self.dataSet["only_special_chars"]["city"],
            street_address=self.dataSet["only_special_chars"]["street_address"],
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string("6", "test_only_special_chars", "True", data)
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string(
                "6", "test_only_special_chars", "False", data
            )
            write_results_from_tests(self.filePath, content)

    def test_code(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username="<script> alert(hello)</script>",
            password=self.dataSet["valid_range_1"]["password"],
            password1=self.dataSet["valid_range_1"]["password1"],
            email=self.dataSet["valid_range_1"]["email"],
            phone_number=self.dataSet["valid_range_1"]["phone_number"],
            country=self.dataSet["valid_range_1"]["country"],
            city=self.dataSet["valid_range_1"]["city"],
            street_address=self.dataSet["valid_range_1"]["street_address"],
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
            content = format_result_string("7", "test_code", "True", data)
            write_results_from_tests(self.filePath1, content)
        except TimeoutException:
            content = format_result_string("7", "test_code", "False", data)
            write_results_from_tests(self.filePath, content)

    def test_extremely_long_input(self):
        self.navigate_to_register_page_and_validate_navigation()
        data = create_test_set(
            username=self.dataSet["extremely_long_input"]["username"],
            password=self.dataSet["extremely_long_input"]["password"],
            password1=self.dataSet["extremely_long_input"]["password1"],
            email=self.dataSet["extremely_long_input"]["email"],
            phone_number=self.dataSet["extremely_long_input"]["phone_number"],
            country=self.dataSet["extremely_long_input"]["country"],
            city=self.dataSet["extremely_long_input"]["city"],
            street_address=self.dataSet["extremely_long_input"]["street_address"],
        )
        self.get_input_field_and_populate_data(**data)
        self.click_submit_button()
        try:
            self.wait.until(lambda driver: driver.title == "Workouts")
            self.assertEqual(self.driver.title, "Workouts")
        except TimeoutException:
            pass
