import os
import pathlib

from selenium.webdriver.common.by import By

from tests.common import BlackBoxTestServer
from tests.common import login_data

here = pathlib.Path(__file__).parent.absolute()
data_dir = here / "data"
backend_port = os.environ.get("BACKEND_PORT", 8000)
frontend_port = os.environ.get("FRONTEND_PORT", 8001)
frontend_address = f"http://localhost:{frontend_port}"


class ViewProfileTest(BlackBoxTestServer):

    headless = True
    fixtures = [
        data_dir / "users.json",
    ]

    def wait_until_value_not_empty(self, by: By, locator: str):
        self.wait.until(lambda _: self.get_input_value(by, locator) != "")

    def get_input_value(self, by: By, locator: str) -> str:
        element = self.driver.find_element(by, locator)
        self.assertIsNotNone(element)
        return element.get_attribute("value")

    def test_non_user_can_not_view_own_profile(self):
        """Test if a user is able to view their own public workout."""
        self.get("profile.html")
        self.assertEqual("My Profile", self.driver.title)
        self.wait_until_value_not_empty(By.ID, "username")
        username_el = self.driver.find_element_by_id("username")
        email_el = self.driver.find_element_by_id("email")
        username = username_el.text
        email = email_el.text
        self.assertEqual(username, "")
        self.assertEqual(email, "")

    def test_user_can_view_own_profile(self):
        """Test if a user is able to view their own public workout."""
        user = login_data["athlete"]
        self.login(user)
        self.get("profile.html")
        self.assertEqual("My Profile", self.driver.title)
        self.wait_until_value_not_empty(By.ID, "username")
        username_el = self.driver.find_element_by_id("username")
        email_el = self.driver.find_element_by_id("email")
        username = username_el.text
        email = email_el.text
        self.assertEqual(username, "Username: arya")
        self.assertEqual(email, "Email: arya@stark.io")
