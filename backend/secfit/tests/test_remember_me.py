import os
import pathlib
import time

from tests.common import BlackBoxTestServer
from tests.common import login_data
from tests.common import LoginData

here = pathlib.Path(__file__).parent.absolute()
data_dir = here / "data"
backend_port = os.environ.get("BACKEND_PORT", 8000)
frontend_port = os.environ.get("FRONTEND_PORT", 8001)
frontend_address = f"http://localhost:{frontend_port}"


class RememberMeTest(BlackBoxTestServer):
    """Test case for the remember me feature.

    When login in with remember me set to true, a remember_me cookie
    should be sent to the user.
    """

    fixtures = [
        data_dir / "users.json",
    ]

    def login_with_remember_me(self, user: LoginData):
        """Log in a user with remember me set to true and wait until redirected
        to the workouts page."""
        self.get("login.html")
        self.assertEqual("Login", self.driver.title)
        username_input = self.driver.find_element_by_name("username")
        username_input.send_keys(user.username)
        password_input = self.driver.find_element_by_name("password")
        password_input.send_keys(user.password)
        remember_me_checkbox = self.driver.find_element_by_id("rememberMe")
        remember_me_checkbox.click()
        time.sleep(3)
        self.driver.find_element_by_id("btn-login").click()
        self.wait.until(lambda driver: driver.title == "Workouts")

    def test_user_does_not_have_remember_me_cookie(self):
        user = login_data["athlete"]
        self.login(user)
        cookies = self.driver.get_cookies()
        cookie_present = False
        for cookie in cookies:
            if cookie["name"] == "remember_me":
                cookie_present = True
        self.assertFalse(cookie_present)

    def test_user_has_remember_me_cookie(self):
        user = login_data["athlete"]
        self.login_with_remember_me(user)
        time.sleep(3)
        cookies = self.driver.get_cookies()
        cookie_present = False
        for cookie in cookies:
            if cookie["name"] == "remember_me":
                cookie_present = True
        self.assertTrue(cookie_present)
