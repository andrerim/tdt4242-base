import json
import os
import pathlib
import sys

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from tests.common import BlackBoxTestServer
from tests.common import login_data

here = pathlib.Path(__file__).parent.absolute()
data_dir = here / "data"

data_sets = {
    "min-": dict(
        name="",
        date=("00000000", "-001"),
        visibility="Public",
        notes="",
        type="Fencing",
        sets=-1,
        number=-1,
    ),
    "min": dict(
        name="n",
        date=("01010001", "0000"),
        visibility="Public",
        notes="n",
        type="Fencing",
        sets=0,
        number=0,
    ),
    "nom": dict(
        name="new workout",
        date=("03152021", "0815"),
        visibility="Public",
        notes="a meaningful note",
        type="Fencing",
        sets=3,
        number=8,
    ),
    "max": dict(
        name="n" * 100,
        date=("12319999", "2359"),
        visibility="Public",
        notes="n" * 100,
        type="Fencing",
        sets=sys.maxsize,
        number=sys.maxsize,
    ),
    "max+": dict(
        name="n" * 101,
        date=("999910000", "2460"),
        visibility="Public",
        notes="n" * 101,
        type="Fencing",
        sets=sys.maxsize + 1,
        number=sys.maxsize + 1,
    ),
}


class WorkoutBoundaryValuesTest(BlackBoxTestServer):
    """Boundary value test case for creating a new workout."""

    headless = False
    fixtures = [
        data_dir / "users.json",
        data_dir / "exercises.json",
    ]
    result_file = here / "results" / "workout-boundary-values.json"

    def populate_workout_form(self, **kwargs):
        for name, value in kwargs.items():
            element = self.driver.find_element(By.NAME, name)
            if name == "date":
                date, time_ = value
                element.send_keys(date, Keys.TAB, time_)
            if name in ["type", "visibility"]:
                select = Select(element)
                select.select_by_visible_text(value)
            else:
                element.send_keys(value)

    def test_boundary_values(self):
        user = login_data["athlete"]
        self.login(user)
        results = {"valid": [], "invalid": []}
        for data in data_sets.values():
            self.get("workout.html")
            self.populate_workout_form(**data)
            self.driver.find_element(By.ID, "btn-ok-workout").click()
            try:
                self.wait.until(lambda driver: driver.title == "Workouts")
                results["valid"].append(data)
            except TimeoutException:
                results["invalid"].append(data)

        result_dir = self.result_file.parent
        if not result_dir.exists():
            os.makedirs(result_dir)

        with open(self.result_file, "w") as f:
            json.dump(results, f, indent=4)
