from django.test import TestCase
from rest_framework.test import APIClient

from comments.models import Comment
from integrationtests import util
from users.models import User
from workouts.models import Workout


class TestAPIPath(TestCase):
    def setUp(self):
        self.client = APIClient()

        util.create_exercises(1)
        util.create_user("tyrion")
        self.user1 = User.objects.get(username="tyrion")
        util.create_workout(self.user1)
        self.workout = Workout.objects.get(id=1)
        util.create_comment(
            **{
                "owner": self.user1,
                "workout": self.workout,
                "content": "This is a comment.",
            }
        )

    def test_get_comments_api(self):
        self.authenticate_and_assert("/api/comments/")

    def test_get_comment_by_id_api(self):
        self.authenticate_and_assert("/api/comments/1/")

    def test_workouts_api(self):
        self.authenticate_and_assert("/api/workouts/")

    def test_workout_id_api(self):
        self.authenticate_and_assert("/api/workouts/1/")

    def test_users_api(self):
        self.authenticate_and_assert("/api/users/")

    def test_users_id_api(self):
        self.authenticate_and_assert("/api/users/1/")

    def authenticate_and_assert(self, api_path):
        self.client.force_authenticate(user=self.user1)
        response = self.client.get(api_path)
        self.assertEqual(response.status_code, 200)
