from django.test import TestCase
from rest_framework.test import APIClient

import util
from users.models import User


class TestFiltering(TestCase):
    def setUp(self):
        self.client = APIClient()

        self.exercise_count = 15
        util.create_exercises(self.exercise_count)

        util.create_user("tyrion")
        self.user = User.objects.get(username="tyrion")
        util.create_workout(self.user)

    def test_get_exercise_types(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get("/api/exercises/")

        self.assertEqual(response.status_code, 200)
        data = response.json()

        self.assertEqual(data["count"], self.exercise_count)
        self.assertGreater(len(data["results"]), 0)

    def test_exercise_types_pagination(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get("/api/exercises/")

        self.assertEqual(response.status_code, 200)

        data = response.json()
        results_count = len(data["results"])
        response = self.client.get(data["next"])

        self.assertEqual(response.status_code, 200)

        data = response.json()
        results_count += len(data["results"])

        self.assertEqual(results_count, self.exercise_count)

    def test_get_workouts(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get("/api/workouts/")
        self.assertEqual(response.status_code, 200)
