from django.test import TestCase
from rest_framework.test import APIClient

import util
from users.models import User


class TestProfile(TestCase):
    def setUp(self):
        self.client = APIClient()

        util.create_user("tyrion")
        util.create_user("arya")
        self.user = User.objects.get(username="arya")

    def test_get_user_data_not_authenticated(self):
        response = self.client.get("/api/users/?user=current")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(data["count"], 0)

    def test_get_user_data(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get("/api/users/?user=current")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        results = data["results"]
        self.assertEqual(len(results), 1)

        user_data = results[0]
        self.assertEqual(user_data["username"], "arya")
        self.assertEqual(user_data["email"], "arya@lannister.com")
