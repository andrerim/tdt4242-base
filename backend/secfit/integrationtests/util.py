from django.utils import timezone

from users.serializers import UserSerializer
from workouts.models import Exercise
from workouts.serializers import ExerciseSerializer
from workouts.serializers import WorkoutSerializer
from comments.serializers import CommentSerializer


def create_user_data(**kwargs):
    data = {
        "username": "",
        "email": "",
        "password": "",
        "password1": "",
        "phone_number": "",
        "country": "",
        "city": "",
        "street_address": "",
    }
    for key, val in kwargs.items():
        if key in data:
            data[key] = val
    return data


def create_exercise_data(**kwargs):
    data = {
        "name": "",
        "description": "",
        "unit": "",
    }
    for key, val in kwargs.items():
        if key in data:
            data[key] = val
    return data


def create_workout_data(**kwargs):
    data = {
        "owner": "",
        "name": "",
        "date": timezone.now(),
        "notes": "",
        "visibility": "",
        "exercise_instances": "",
    }
    for key, val in kwargs.items():
        if key in data:
            data[key] = val
    return data


def create_user(username):
    user_serializer = UserSerializer()
    valid_data = create_user_data(
        username=username,
        email=f"{username}@lannister.com",
        password="iDrInKaNdIkNoWtHiNgS",
        password1="iDrInKaNdIkNoWtHiNgS",
    )
    user_serializer.create(valid_data)


def create_exercises(amount):
    exerciseSerializer = ExerciseSerializer()
    for i in range(amount):
        valid_data = create_exercise_data(
            name="Squats" + str(i), description="Bend knees" + str(i), unit="kg"
        )
        exerciseSerializer.create(valid_data)


def create_workout(user):
    workoutSerializer = WorkoutSerializer()
    test = Exercise.objects.get(id="1")
    valid_data = create_workout_data(
        owner=user,
        name="Super big hard workout",
        notes="It was easy",
        visibility="PU",
        exercise_instances=[{"exercise": test, "sets": 123, "number": 123}],
    )
    workoutSerializer.create(valid_data)


def create_comment(**kwargs):
    comment_serializer = CommentSerializer()
    data = {
        "owner": "",
        "workout": "",
        "content": "",
        "timestamp": timezone.now(),
    }

    for key, val in kwargs.items():
        if key in data:
            data[key] = val

    comment_serializer.create(data)
