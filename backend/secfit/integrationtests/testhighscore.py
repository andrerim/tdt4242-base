from django.test import TestCase
from rest_framework.test import APIClient

import util
from users.models import User


class TestHighScore(TestCase):
    def setUp(self):
        self.client = APIClient()

        self.exercise_count = 15
        util.create_exercises(self.exercise_count)

        util.create_user("tyrion")
        self.user1 = User.objects.get(username="tyrion")
        self.user1_workout_instances = 5
        for _ in range(self.user1_workout_instances):
            util.create_workout(self.user1)

        util.create_user("jamie")
        self.user2 = User.objects.get(username="jamie")
        self.user2_workout_instances = 2
        for _ in range(self.user2_workout_instances):
            util.create_workout(self.user2)

        util.create_user("arya")
        self.user3 = User.objects.get(username="arya")
        self.user3_workout_instances = 3
        for _ in range(self.user3_workout_instances):
            util.create_workout(self.user3)

    def test_get_highscores(self):
        self.client.force_authenticate(user=self.user1)
        response = self.client.get("/api/highscores/")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        highscores = len(data["results"])
        self.assertEqual(highscores, 3)

    def test_get_highscore_for_non_existing_exercise(self):
        self.client.force_authenticate(user=self.user1)
        response = self.client.get("/api/highscores/?exercise=2")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        results = len(data["results"])
        self.assertEqual(results, 0)

    def test_get_highscore_for_exercise(self):
        self.client.force_authenticate(user=self.user1)
        response = self.client.get("/api/highscores/?exercise=1")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        results = data["results"]

        for res in results:
            if res["username"] == "tyrion":
                self.assertEqual(res["score"], self.user1_workout_instances)
            elif res["username"] == "jamie":
                self.assertEqual(res["score"], self.user2_workout_instances)
            elif res["username"] == "arya":
                self.assertEqual(res["score"], self.user3_workout_instances)
            else:
                self.assertFalse(
                    True, "User requested does not exist. Add users to test dataset"
                )

    def test_highscore_ranking(self):
        self.client.force_authenticate(user=self.user1)
        response = self.client.get("/api/highscores/?exercise=1")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        results = data["results"]
        firstplace = results[0]["username"]
        secondplace = results[1]["username"]
        thirdplace = results[2]["username"]

        self.assertEqual(firstplace, "tyrion")
        self.assertEqual(secondplace, "arya")
        self.assertEqual(thirdplace, "jamie")
