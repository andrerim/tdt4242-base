async function fetchWorkouts(ordering) {
    const response = await sendRequest(
        "GET",
        `${HOST}/api/workouts/?ordering=${ordering}`
    );

    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        const data = await response.json();
        const workouts = data.results;
        const container = document.getElementById("div-content");

        workouts.forEach((workout) => {
            const templateWorkout = document.querySelector("#template-workout");
            const cloneWorkout = templateWorkout.content.cloneNode(true);

            const aWorkout = cloneWorkout.querySelector("a");
            aWorkout.href = `workout.html?id=${workout.id}`;

            const exerciseIds = workout.exercise_instances.map((instance) => {
                const parts = instance.exercise.split("/");
                return parseInt(parts[parts.length - 2]);
            });
            aWorkout.dataset.exerciseIds = exerciseIds;

            const h5 = aWorkout.querySelector("h5");
            h5.textContent = workout.name;

            const localDate = new Date(workout.date);

            const table = aWorkout.querySelector("table");
            const rows = table.querySelectorAll("tr");
            rows[0].querySelectorAll(
                "td"
            )[1].textContent = localDate.toLocaleDateString();
            rows[1].querySelectorAll(
                "td"
            )[1].textContent = localDate.toLocaleTimeString();
            rows[2].querySelectorAll("td")[1].textContent =
                workout.owner_username;
            rows[3].querySelectorAll("td")[1].textContent =
                workout.exercise_instances.length;

            container.appendChild(aWorkout);
        });
        return workouts;
    }
}

function createWorkout() {
    window.location.replace("workout.html");
}

async function populateExerciseFilter() {
    const selectFilter = document.getElementById("filter-exercise");
    const exerciseList = await fetchAllExercises();
    exerciseList.forEach((exercise) => {
        const option = document.createElement("option");
        option.value = exercise.id;
        option.innerText = exercise.name;
        selectFilter.append(option);
    });
}

window.addEventListener("DOMContentLoaded", async () => {
    const createButton = document.querySelector("#btn-create-workout");
    createButton.addEventListener("click", createWorkout);
    let ordering = "-date";

    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has("ordering")) {
        const aSort = null;
        ordering = urlParams.get("ordering");
        if (ordering == "name" || ordering == "owner" || ordering == "date") {
            const aSort = document.querySelector(
                `a[href="?ordering=${ordering}"`
            );
            aSort.href = `?ordering=-${ordering}`;
        }
    }

    const currentSort = document.querySelector("#current-sort");
    currentSort.innerHTML = `${
        ordering.startsWith("-") ? "Descending" : "Ascending"
    } ${ordering.replace("-", "")}`;

    const currentUser = await getCurrentUser();

    if (ordering.includes("owner")) {
        ordering += "__username";
    }

    const workouts = await fetchWorkouts(ordering);
    const workoutElements = document.querySelectorAll(".workout");
    const workoutsMap = workouts.map((workout, i) => [
        workout,
        workoutElements[i],
    ]);

    await populateExerciseFilter();
    let activeFilterTab = null;

    const filterTabs = document.querySelectorAll("a[data-bs-toggle='list']");
    [...filterTabs].forEach((tab) => {
        if (tab.classList.contains("active")) {
            activeFilterTab = tab;
        }
        tab.addEventListener("show.bs.tab", (event) => {
            activeFilterTab = event.currentTarget;
            applyFilter();
        });
    });
    const exerciseFilter = document.querySelector("#filter-exercise");
    exerciseFilter.addEventListener("change", () => applyFilter());

    function applyFilter() {
        const workoutFilter = activeFilterTab.id;
        const exerciseId = exerciseFilter.value;

        workoutsMap.forEach(([workout, element]) => {
            const exerciseIds = element.dataset.exerciseIds.split(",");
            const matchesExercise =
                exerciseId < 0 || exerciseIds.includes(exerciseId);

            switch (workoutFilter) {
                case "list-my-workouts-list":
                    if (workout.owner == currentUser.url && matchesExercise) {
                        element.classList.remove("hide");
                    } else {
                        element.classList.add("hide");
                    }
                    break;
                case "list-athlete-workouts-list":
                    if (
                        currentUser.athletes &&
                        currentUser.athletes.includes(workout.owner) &&
                        matchesExercise
                    ) {
                        element.classList.remove("hide");
                    } else {
                        element.classList.add("hide");
                    }
                    break;
                case "list-public-workouts-list":
                    if (workout.visibility == "PU" && matchesExercise) {
                        element.classList.remove("hide");
                    } else {
                        element.classList.add("hide");
                    }
                    break;
                default:
                    if (matchesExercise) {
                        element.classList.remove("hide");
                    } else {
                        element.classList.add("hide");
                    }
                    break;
            }
        });
    }
});
