function setColumnVisibility(table, column, visible) {
    table.querySelectorAll("tr").forEach((row) => {
        const cell = row.children[column];
        if (visible) {
            cell.classList.remove("hidden");
        } else {
            cell.classList.add("hidden");
        }
    });
}

function compareScore(a, b) {
    if (a.score < b.score) {
        return 1;
    }
    if (b.score < a.score) {
        return -1;
    }
    return 0;
}

function compareUnits(a, b) {
    if (a.units < b.units) {
        return 1;
    }
    if (b.units < a.units) {
        return -1;
    }
    return 0;
}

async function fetchHighscores(exerciseId = undefined) {
    const withExercise = exerciseId && exerciseId > -1;
    const endpoint = withExercise
        ? `${HOST}/api/highscores/?exercise=${exerciseId}`
        : `${HOST}/api/highscores/`;

    const response = await sendRequest("GET", endpoint);

    if (response.ok) {
        const data = await response.json();
        const highscores = data.results;
        const table = document.getElementById("highscore-table");
        const container = table.querySelector("#highscore-content");
        const rowTemplate = document.getElementById("template-highscore-row");
        const rankByUnits = document.getElementById("rank-by-units");

        const renderHighscores = () => {
            container.innerText = "";
            const compareFunc = rankByUnits.checked
                ? compareUnits
                : compareScore;
            highscores.sort(compareFunc);
            highscores.forEach((highscore, i) => {
                const row = rowTemplate.content.cloneNode(true);
                const cells = row.querySelectorAll("td");
                cells[0].textContent = i + 1;
                cells[1].textContent = highscore.username;
                cells[2].textContent = highscore.score;
                cells[3].textContent = highscore.units ? highscore.units : "";
                container.appendChild(row);
            });
            setColumnVisibility(table, 3, withExercise);
        };

        rankByUnits.addEventListener("change", () => renderHighscores());
        renderHighscores();
    }

    return response;
}

async function populateExerciseFilter() {
    const selectFilter = document.getElementById("filter-exercise");
    const exerciseList = await fetchAllExercises();
    exerciseList.forEach((exercise) => {
        const option = document.createElement("option");
        option.value = exercise.id;
        option.innerText = exercise.name;
        selectFilter.append(option);
    });
}

window.addEventListener("DOMContentLoaded", async () => {
    await populateExerciseFilter();
    const rankContainer = document.getElementById("rank-container");
    const selectFilter = document.getElementById("filter-exercise");
    selectFilter.addEventListener("change", async (event) => {
        if (event.target.value) {
            rankContainer.classList.remove("invisible");
            rankContainer.querySelector("#rank-by-units").disabled = false;
        } else {
            rankContainer.classList.add("invisible");
            rankContainer.querySelector("#rank-by-units").disabled = true;
        }
        const response = await fetchHighscores(event.target.value);
        if (!response.ok) {
            const data = await response.json();
            const alert = createAlert("Could not retrieve highscores!", data);
            document.body.prepend(alert);
        }
    });
    selectFilter.dispatchEvent(new Event("change"));
});
