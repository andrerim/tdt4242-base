window.addEventListener("DOMContentLoaded", async () => {
    const data = await getCurrentUser();

    if (!data) {
        const alert = document.querySelector(".profile-card-container");
        alert.innerHTML = "Could not retrive user data";
    } else {
        const selectFilter = document.querySelector("#username");
        selectFilter.innerHTML = `Username: ${data.username}`;
        const email = document.querySelector("#email");
        email.innerHTML = `Email: ${data.email}`;
    }
});
